��    	      d      �       �   �   �   .   h     �     �     �  !   �     �       �  %  ~   �  /   &     V     n     w  !   �     �     �                                   	           Confirmo que el meu podcast compleix amb les <a href="https://xrcb.cat/ca/politica-de-privadesa/">condicions d&#39;ús</a> de la XRCB. De què va el teu podcast? descriu-lo breument En quin idioma està? Llest! Posa un títol al podcast Selecciona unes quantes etiquetes Tria el teu arxiu .mp3 i puja'l Tria el teu programa Project-Id-Version: xrcb-plugin
POT-Creation-Date: 2020-05-28 17:01+0200
PO-Revision-Date: 2020-05-28 17:01+0200
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: xrcb-plugin.php
 Confirmo que mi podcast cumple con las <a href="https://xrcb.cat/es/politica-de-privadesa/">condiciones de uso</a> de la XRCB. ¿De qué va tu podcast? descríbelo brevemente ¿En qué idioma está? ¡Listo! Pon un título a tu podcast Selecciona unas cuantas etiquetas Elige tu archivo .mp3 y súbelo Elige tu programa 