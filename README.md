# XRCB API Wordpress Plugin

Wordpress plugin for https://xrcb.cat/

This plugins sould contain custom post types, field definitions with ACF and site funcionalities.

For now, all the post types are defined here.

## Custom post type `radio`

All registers of this post type are shown on the map. Actually it includes this fields:

- Title
- Category
- Neighbourhood
- Year of foundation
- Short history
- License
- Web
- Mail
- Address: This fields includes address as a text field plus latitude and longitude. It's the format used by Google Maps to make it compatible with the optional [ACF Google Maps field](https://www.advancedcustomfields.com/resources/google-map/):
```array( 'address' => 'xxx', 'lat' => 'xxx', 'lng' => 'xxx' )```

At least the title and fields `latitude` and `longitude` have to be defined. The post type can easily be extended adding fields to post type definition of `radio` at `functions.php` and modifying template `content-single-radio.php`. 

Contents of type `radio` in the backend are listed under Radios menu.

## Custom post type `podcast`

It has the following fields:

- Title
- Description
- Radio
- Date of broadcasting
- File MP3

The post type can easily be extended adding fields to post type definition of `podcast` at `functions.php` and modifying template `content-single-podcast.php`. 

Contents of type `podcast` in the backend are listed under Podcasts menu.

## Custom post type `resource`

It has the following fields:

- URL

The post type can easily be extended adding fields to post type definition of `resource` at `functions.php` and modifying template `content-single-resource.php`. 

Contents of type `resource` in the backend are listed under Recursos menu.