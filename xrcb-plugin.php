<?php
/**
 * @package xrcb-plugin
 * @version 1.0.0
 */
/*
Plugin Name: XRCB Plugin
Plugin URI: https://gitlab.com/guifi-exo/xrcb/xrcb-plugin
Description: This plugins contains custom post types, field definitions with ACF and funcionality for https://xrcb.cat/
Author: Gerald Kogler
Version: 1.0.0
Author URI: http://go.yuri.at/
*/

/**
 * Make theme available for translation
 * Translations can be filed in the /languages/ directory
 */
load_plugin_textdomain( 'xrcb-plugin', false, dirname(plugin_basename(__FILE__)) . '/languages' );

/**
 * RADIO post type export
*/
function xrcb_register_my_cpts_radio() {

	$labels = array(
		"name" => __( "Radios", "xrcb-plugin" ),
		"singular_name" => __( "Radio", "xrcb-plugin" ),
	);

	register_taxonomy( 'radio_category', 'radio', array(
		'label'		   => 'Radio Categories',
        'rewrite'      => array( 'slug' => 'radio_category' ),
        'capabilities' => array(
			'assign_terms' => 'edit_radios',
			'edit_terms' => 'publish_radios'
		),
		'show_in_rest' => true
    ) );

	register_taxonomy( 'radio_tag', 'radio', array(
        'rewrite'      => array( 'slug' => 'radio_tag' ),
        'capabilities' => array(
			'assign_terms' => 'edit_radios',
			'edit_terms' => 'publish_radios'
		),
		'show_in_rest' => true
    ) );

	$args = array(
		"label" => __( "Radios", "xrcb-plugin" ),
		"labels" => $labels,
		"description" => "Radio station",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "radios",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "radio",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "radio", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-controls-volumeon",
		"supports" => array( "title", "author", "thumbnail" ),
		"taxonomies" => array( "radio_category", "radio_tag" ),
	);

	register_post_type( "radio", $args );
}
add_action( 'init', 'xrcb_register_my_cpts_radio' );

/**
 * remove category metaboxes for Radios
*/
function xrcb_remove_metaboxes() {
	remove_meta_box( 'tagsdiv-radio_category', 'radio', 'normal' );
}
add_action( 'admin_menu', 'xrcb_remove_metaboxes' );

/**
 * PODCAST post type export
*/
function xrcb_register_my_cpts_podcast() {

	$labels = array(
		"name" => __( "Podcasts", "xrcb-plugin" ),
		"singular_name" => __( "Podcast", "xrcb-plugin" ),
	);

	register_taxonomy( 'podcast_category', 'podcast', array(
		'label'		   => 'Podcast Categories',
        'rewrite'      => array( 'slug' => 'podcast_category' ),
        'capabilities' => array(
			'assign_terms' => 'edit_podcasts',
			'edit_terms' => 'publish_podcasts'
		),
		'show_in_rest' => true
    ) );

	register_taxonomy( 'podcast_tag', 'podcast', array(
        'rewrite'      => array( 'slug' => 'podcast_tag' ),
        'capabilities' => array(
			'assign_terms' => 'edit_podcasts',
			'edit_terms' => 'publish_podcasts',
			'manage_terms' => 'publish_podcasts',
			'delete_terms' => 'publish_podcasts'
		),
		'show_in_rest' => true
    ) );

	register_taxonomy( 'podcast_programa', 'podcast', array(
		'label'		   => 'Programes',
        'rewrite'      => array( 'slug' => 'podcast_programa' ),
        'capabilities' => array(
			'assign_terms' => 'edit_podcasts',
			'edit_terms' => 'publish_podcasts',
			'manage_terms' => 'publish_podcasts',
			'delete_terms' => 'publish_podcasts'
		),
		'show_in_rest' => true
    ) );

	register_taxonomy( 'podcast_radio', 'podcast', array(
		'label'		   => 'Radio',
        'rewrite'      => array( 'slug' => 'podcast_radio' ),
        'capabilities' => array(
			'assign_terms' => 'edit_podcasts',
			'edit_terms' => 'publish_podcasts',
			'manage_terms' => 'publish_podcasts',
			'delete_terms' => 'publish_podcasts'
		)
    ) );

	$args = array(
		"label" => __( "Podcasts", "xrcb-plugin" ),
		"labels" => $labels,
		"description" => "xrcb podcast",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "podcasts",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "podcast",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "podcast", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-format-audio",
		"supports" => array( "title", "editor", "author" ),
		"taxonomies" => array( "podcast_category", "podcast_tag", "podcast_programa", "podcast_radio"  ),
	);

	register_post_type( "podcast", $args );
}
add_action( 'init', 'xrcb_register_my_cpts_podcast' );

/**
 * Make podcasts show up in archive pages
*/
/*function xrcb_add_custom_post_types_to_query( $query ) {
	if( 
		is_archive() &&
		$query->is_main_query() &&
		empty( $query->query_vars['suppress_filters'] )
	) {
		$query->set( 'post_type', array( 
			'post',
			'podcast',
			'radio',
			'resource'
		) );
	}
}
add_filter( 'pre_get_posts', 'xrcb_add_custom_post_types_to_query' );*/

/**
 * Resources post type export
 */
function xrcb_register_my_cpts_resource() {

	$labels = array(
		"name" => __( "Recursos", "xrcb-plugin" ),
		"singular_name" => __( "Recurs", "xrcb-plugin" ),
	);

	register_taxonomy( 'resource_tag', 'resource', array(
        'rewrite'      => array( 'slug' => 'resource_tag' ),
        'capabilities' => array(
			'assign_terms' => 'edit_resources',
			'edit_terms' => 'publish_resources'
		)
    ) );

	$args = array(
		"label" => __( "Recursos", "xrcb-plugin" ),
		"labels" => $labels,
		"description" => "xrcb recursos",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "resources",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "resource",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "resource", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-playlist-audio",
		"supports" => array( "title", "editor", "author", "thumbnail", "excerpt" ),
		"taxonomies" => array( "resource_tag" ),
	);

	register_post_type( "resource", $args );
}
add_action( 'init', 'xrcb_register_my_cpts_resource' );


/**
 * Resources post type export
 */
function xrcb_register_my_cpts_repost() {

	$labels = array(
		"name" => __( "Reposts", "xrcb-plugin" ),
		"singular_name" => __( "Repost", "xrcb-plugin" ),
	);

	$args = array(
		"label" => __( "Reposts", "xrcb-plugin" ),
		"labels" => $labels,
		"description" => "xrcb reposts",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "reposts",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "repost",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "repost", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-controls-repeat",
		"supports" => array( "author" ),
	);

	register_post_type( "repost", $args );
}
add_action( 'init', 'xrcb_register_my_cpts_repost' );

/**
 * ACF field group export
 */
//include_once('advanced-custom-fields/acf.php');
if(function_exists("register_field_group")) {
	register_field_group(array (
		'id' => 'acf_radio',
		'title' => 'Radio',
		'fields' => array (
			array (
				'key' => 'field_5b2a115exabc',
				'label' => 'Radio Categories',
				'name' => 'categories',
				'type' => 'taxonomy',
				'field_type' => 'radio',
				'required' => 1,
				'load_save_terms' => 1,
				'taxonomy' => 'radio_category',
				'instructions' => 'Escoge de las categorías predeterminadas.',
			),
			array (
				'key' => 'field_5a685b74baaa2',
				'label' => 'Ciudad',
				'name' => 'ciudad',
				'type' => 'select',
				'ui' => 1,
				'ajax' => 1,
				'required' => 1,
				'choices' => array (
					// para cualquier cambio en ciudad o barrio hace necesario
					// cambios en xrcb-theme/js/autopopulate.js
					'Barcelona' => 'Barcelona',
					'Hospitalet de Llobregat' => 'Hospitalet de Llobregat',
					'Esplugues de Llobregat' => 'Esplugues de Llobregat',
					'El Prat de Llobregat' => 'El Prat de Llobregat',
					'Altres' => 'Altres'
				),
				'default_value' => '',
			),
			array (
				'key' => 'field_5a685b74baee2',
				'label' => 'Barrio',
				'name' => 'barrio',
				'type' => 'select',
				'ui' => 1,
				'ajax' => 1,
				'required' => 1,
				'choices' => array (
					// Barcelona
			        "el Raval" => "el Raval",
			        "el Barri Gòtic" => "el Barri Gòtic",
			        "la Barceloneta" => "la Barceloneta",
			        "Sant Pere, Santa Caterina i la Ribera" => "Sant Pere, Santa Caterina i la Ribera",
			        "el Fort Pienc" => "el Fort Pienc",
			        "la Sagrada Família" => "la Sagrada Família",
			        "la Dreta de l\'Eixample" => "la Dreta de l\'Eixample",
			        "l\'Antiga Esquerra de l\'Eixample" => "l\'Antiga Esquerra de l\'Eixample",
			        "la Nova Esquerra de l\'Eixample" => "la Nova Esquerra de l\'Eixample",
			        "Sant Antoni" => "Sant Antoni",
			        "el Poble Sec" => "el Poble Sec",
			        "la Marina del Prat Vermell" => "la Marina del Prat Vermell",
			        "la Marina de Port" => "la Marina de Port",
			        "la Font de la Guatlla" => "la Font de la Guatlla",
			        "Hostafrancs" => "Hostafrancs",
			        "la Bordeta" => "la Bordeta",
			        "Sants - Badal" => "Sants - Badal",
			        "Sants" => "Sants",
			        "les Corts" => "les Corts",
			        "la Maternitat i Sant Ramon" => "la Maternitat i Sant Ramon",
			        "Pedralbes" => "Pedralbes",
			        "Vallvidrera, el Tibidabo i les Planes" => "Vallvidrera, el Tibidabo i les Planes",
			        "Sarrià" => "Sarrià",
			        "les Tres Torres" => "les Tres Torres",
			        "Sant Gervasi - la Bonanova" => "Sant Gervasi - la Bonanova",
			        "Sant Gervasi - Galvany" => "Sant Gervasi - Galvany",
			        "el Putxet i el Farró" => "el Putxet i el Farró",
			        "Vallcarca i els Penitents" => "Vallcarca i els Penitents",
			        "el Coll" => "el Coll",
			        "la Salut" => "la Salut",
			        "la Vila de Gràcia" => "la Vila de Gràcia",
			        "el Camp d\'en Grassot i Gràcia Nova" => "el Camp d\'en Grassot i Gràcia Nova",
			        "el Baix Guinardó" => "el Baix Guinardó",
			        "Can Baró" => "Can Baró",
			        "el Guinardó" => "el Guinardó",
			        "la Font d\'en Fargues" => "la Font d\'en Fargues",
			        "el Carmel" => "el Carmel",
			        "la Teixonera" => "la Teixonera",
			        "Sant Genís dels Agudells" => "Sant Genís dels Agudells",
			        "Montbau" => "Montbau",
			        "la Vall d\'Hebron" => "la Vall d\'Hebron",
			        "la Clota" => "la Clota",
			        "Horta" => "Horta",
			        "Vilapicina i la Torre Llobeta" => "Vilapicina i la Torre Llobeta",
			        "Porta" => "Porta",
			        "el Turó de la Peira" => "el Turó de la Peira",
			        "Can Peguera" => "Can Peguera",
			        "la Guineueta" => "la Guineueta",
			        "Canyelles" => "Canyelles",
			        "les Roquetes" => "les Roquetes",
			        "Verdun" => "Verdun",
			        "la Prosperitat" => "la Prosperitat",
			        "la Trinitat Nova" => "la Trinitat Nova",
			        "Torre Baró" => "Torre Baró",
			        "Ciutat Meridiana" => "Ciutat Meridiana",
			        "Vallbona" => "Vallbona",
			        "la Trinitat Vella" => "la Trinitat Vella",
			        "Baró de Viver" => "Baró de Viver",
			        "el Bon Pastor" => "el Bon Pastor",
			        "Sant Andreu" => "Sant Andreu",
			        "la Sagrera" => "la Sagrera",
			        "el Congrés i els Indians" => "el Congrés i els Indians",
			        "Navas" => "Navas",
			        "el Camp de l\'Arpa del Clot" => "el Camp de l\'Arpa del Clot",
			        "el Clot" => "el Clot",
			        "el Parc i la Llacuna del Poblenou" => "el Parc i la Llacuna del Poblenou",
			        "la Vila Olímpica del Poblenou" => "la Vila Olímpica del Poblenou",
			        "el Poblenou" => "el Poblenou",
			        "Diagonal Mar i el Front Marítim del Poblenou" => "Diagonal Mar i el Front Marítim del Poblenou",
			        "el Besòs i el Maresme" => "el Besòs i el Maresme",
			        "Provenals del Poblenou" => "Provenals del Poblenou",
			        "Sant Martí de Provenals" => "Sant Martí de Provenals",
			        "la Verneda i la Pau" => "la Verneda i la Pau",
			        // Hospitalet de Llobregat
			        "El Centre" => "El Centre",
			        "Sant Josep" => "Sant Josep",
			        "Sanfeliu" => "Sanfeliu",
			        "Collblanc" => "Collblanc",
			        "La Torrassa" => "La Torrassa",
			        "Santa Eulalia" => "Santa Eulalia",
			        "Granvia Sur" => "Granvia Sur",
			        "La Florida" => "La Florida",
			        "Les Planes" => "Les Planes",
			        "Pubilla Casas" => "Pubilla Casas",
			        "Can Serra" => "Can Serra",
			        "Bellvitge" => "Bellvitge",
			        "El Gornal" => "El Gornal",
			        "Distrito Económico" => "Distrito Económico",
			        // Esplugues de Llobregat
			        "El Gall" => "El Gall",
			        "La Plana" => "La Plana",
			        "Montesa" => "Montesa",
			        "Can Vidalet" => "Can Vidalet",
			        "El Centre" => "El Centre",
			        "Can Clota" => "Can Clota",
			        "Ciutat Diagonal" => "Ciutat Diagonal",
			        "Finestrelles" => "Finestrelles",
			        "La Miranda" => "La Miranda",
			        "La Mallola" => "La Mallola",
			        // Altres
			        "Guinea Ecuatorial" => "Guinea Ecuatorial"
				),
				'default_value' => '',
			),
			array (
				'key' => 'field_5a68588bef9e4',
				'label' => 'Sede',
				'name' => 'sede',
				'type' => 'radio',
				'choices' => array (
					'fija' => 'fija',
					'itinerante' => 'itinerante',
					'fantasma' => 'fantasma',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'fija',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a691fa29d5db',
				'label' => 'Dirección',
				'name' => 'location',
				'type' => 'google_map',
				'center_lat' => '41.3860',
				'center_lng' => '2.1553',
				'zoom' => 13,
				'height' => 400,
			),
			array (
				'key' => 'field_5a685812ef9e2',
				'label' => 'Año de fundación',
				'name' => 'anyo_fundacion',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 1900,
				'max' => 2100,
				'step' => 1,
			),
			array (
				'key' => 'field_5a6857bcef9e1',
				'label' => 'Breu historia (CAT)',
				'name' => 'historia',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'new_lines' => 'br',
			),
			array (
				'key' => 'field_5a6857bcef9e2',
				'label' => 'Breve historia (ES)',
				'name' => 'historia_es',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'new_lines' => 'br',
			),
			array (
				'key' => 'field_5a6857bcef9e3',
				'label' => 'Brief biography (EN)',
				'name' => 'historia_en',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'new_lines' => 'br',
			),
			array (
				'key' => 'field_5a685849ef9e3',
				'label' => 'Licencia de uso',
				'name' => 'licencia',
				'type' => 'radio',
				'choices' => array (
					'CC' => 'CC',
					'CR' => 'CR',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5a685a93f9d33',
				'label' => 'Web',
				'name' => 'web',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a68631947e47',
				'label' => 'Mail',
				'name' => 'mail',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'instructions' => 'Mail público de la radio.',
				'instruction_placement' => 'field',
			),
			array (
				'key' => 'field_5a685812efxxx',
				'label' => 'Frecuencia FM',
				'name' => 'fm',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 87.5,
				'max' => 108,
				'step' => 0.1,
			),
			array (
				'key' => 'field_5a685a93f9126',
				'label' => 'Títol Convocatoria (CAT)',
				'name' => 'convocatoriatitle',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685a93f9123',
				'label' => 'URL Convocatoria (CAT)',
				'name' => 'convocatoria',
				'type' => 'url',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685a93f9127',
				'label' => 'Título Convocatoria (ES)',
				'name' => 'convocatoriatitle_es',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685a93f9124',
				'label' => 'URL Convocatoria (ES)',
				'name' => 'convocatoria_es',
				'type' => 'url',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685a93f9128',
				'label' => 'Title Convocatoria (EN)',
				'name' => 'convocatoriatitle_en',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685a93f9125',
				'label' => 'URL Convocatoria (EN)',
				'name' => 'convocatoria_en',
				'type' => 'url',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5a685849efxyz',
				'label' => __('En quin idioma està?', 'xrcb-plugin'),
				'name' => 'idioma_podcast',
				'instructions' => 'Idioma en qual apareixerà la descripció del podcast.',
				'type' => 'radio',
				'required' => 1,
				'choices' => array (
					'ca' => 'ca',
					'es' => 'es',
					'en' => 'en',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'ca',
			),
			array (
				'key' => 'field_5b2a115e1exxx',
				'label' => 'Podcast Image',
				'name' => 'img_podcast',
				'instructions' => 'Imagen JPG o PNG de 1400x1400px (min) a 3000x3000px (max) usado por apps de podcasts.',
				'type' => 'file',
				'save_format' => 'object',
				'library' => 'uploadedTo',
			),
			array (
				'key' => 'field_5b2a115e1ekkk',
				'label' => 'Embed Player Image',
				'name' => 'img_embed',
				'instructions' => 'Imagen JPG o PNG de 100x29px (exacto) usado para personalizar el player embed.',
				'type' => 'file',
				'save_format' => 'object',
				'library' => 'uploadedTo',
			),
			array(
				'key' => 'field_60089f273311c',
				'label' => 'Usuari de Twitter',
				'name' => 'twitter',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '@',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_60089f273322c',
				'label' => 'Usuari de Instagram',
				'name' => 'instagram',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '@',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_6009a9a56f201',
				'label' => 'Imatges per GIF animat',
				'name' => 'imatges_gif_animat',
				'type' => 'group',
				'instructions' => 'Cinc imatges de 400px d\'ample per 250px d\'alt. Amb les fotos farem un gif animat que serà la imatge del vostre perfil a la xarxa.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_6009a9f96f203',
						'label' => 'Imatge 1',
						'name' => 'imatge_1',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array(
						'key' => 'field_6009a9f96f204',
						'label' => 'Imatge 2',
						'name' => 'imatge_2',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array(
						'key' => 'field_6009a9f96f205',
						'label' => 'Imatge 3',
						'name' => 'imatge_3',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array(
						'key' => 'field_6009a9f96f206',
						'label' => 'Imatge 4',
						'name' => 'imatge_4',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
			array (
				'key' => 'field_5a685849efxxx',
				'label' => '',
				'name' => 'buenaspracticas',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => __('Confirmo que el meu podcast compleix amb les <a href=https://preprod.xrcb.cat/ca/buenas-practicas/>bones pràctiques</a> de la XRCB.', 'xrcb-plugin'),
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
			array (
				'key' => 'field_5a685849efyyy',
				'label' => '',
				'name' => 'privacidad',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => __('Confirmo que el meu podcast compleix amb les <a href=https://xrcb.cat/ca/politica-de-privadesa/>condicions d&#39;ús</a> de la XRCB.', 'xrcb-plugin'),
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
			array (
				'key' => 'field_5a685849efzzz',
				'label' => '',
				'name' => 'ipsave',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => __('Confirmamos que la Radio solamente tenga contenidos libres de conflictos de propiedad intelectual y volvemos a abrir la radio al público.', 'xrcb-plugin'),
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'radio',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_podcast',
		'title' => 'Podcast',
		'fields' => array (
			array (
				'key' => 'field_5b2a115e1e33f',
				'label' => __("Tria el teu arxiu .mp3 i puja'l", "xrcb-plugin"),
				'name' => 'file_mp3',
				'type' => 'file',
				'required' => 0,
				'save_format' => 'object',
				'library' => 'uploadedTo',
				'mime_types' => 'mp3',
				/*'conditional_logic' => [
                    [
                        [
                            "field" => "field_5cff743673e4e",
                            "operator" => "==",
                            "value" => false
                        ]
                    ]
                ],*/
			),
			array (
				'key' => 'field_5b2a14e0b2380',
				'label' => 'Radio',
				'name' => 'radio',
				'type' => 'post_object',
				'required' => 0,
				'post_type' => array (
					0 => 'radio',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 1,
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b2a115exxxx',
				'label' => 'Podcast Categories',
				'name' => 'categories',
				'type' => 'taxonomy',
				'field_type' => 'multi_select',
				'taxonomy' => 'podcast_category',
				'required' => 0,
				'load_save_terms' => 1,
				'instructions' => 'Escoge de las categorías predeterminadas.',
				'allow_null' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
			array (
				'key' => 'field_5b2a115exxx1',
				'label' => __('Selecciona unes quantes etiquetes', 'xrcb-plugin'),
				'name' => 'etiquetes',
				'type' => 'taxonomy',
				'field_type' => 'multi_select',
				'required' => 1,
				'add_term' => 1,
				'load_save_terms' => 1,
				'taxonomy' => 'podcast_tag',
				'instructions' => 'O crea les teves pròpies etiquetes',
			),
			array (
				'key' => 'field_5b2a115exxx2',
				'label' => __('Tria el teu programa', 'xrcb-plugin'),
				'name' => 'programes',
				'type' => 'taxonomy',
				'field_type' => 'select',
				'required' => 0,
				'load_save_terms' => 1,
				'taxonomy' => 'podcast_programa',
			),
			array (
				'key' => 'field_5b2a13f21e342',
				'label' => 'Fecha de emisión',
				'name' => 'fecha_emision',
				'type' => 'date_picker',
				'display_format' => 'd/m/Y',
				'return_format' => 'd/m/Y',
				'first_day' => 1,
				'instructions' => 'Fecha inicial de realización.',
			),
			array (
				'key' => 'field_5a685849efxxx',
				'label' => __('En quin idioma està?', 'xrcb-plugin'),
				'name' => 'idioma',
				'type' => 'select',
				'required' => 1,
				'choices' => array (
					'' => '',
					'ca' => 'ca',
					'es' => 'es',
					'en' => 'en',
					'multi' => 'multi language',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
			),
			array (
				'key' => 'field_5a685849efhhh',
				'label' => __('De què va el teu podcast? descriu-lo breument', 'xrcb-plugin'),
				'name' => 'description',
				'type' => 'wysiwyg',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 0,
			),
			array (
				'key' => 'field_5b2a115e1eayu',
				'label' => 'Imatge Podcast',
				'name' => 'img_podcast',
				'instructions' => 'Imagen JPG o PNG de 1400x1400px (min) a 3000x3000px (max) usada por apps de podcasts y mostrada en la ficha del podcast.',
				'type' => 'file',
				'save_format' => 'object',
				'library' => 'uploadedTo',
			),
			array (
				'key' => 'field_5a685849efabc',
				'label' => '',
				'name' => 'privacidad',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => __('Confirmo que el meu podcast compleix amb les <a href=https://xrcb.cat/ca/politica-de-privadesa/>condicions d&#39;ús</a> de la XRCB.', 'xrcb-plugin'),
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
			array (
				'key' => 'field_5a685849efabc',
				'label' => '',
				'name' => 'ipsave_podcast',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => __('Confirmamos que este Podcast está libre de conflictos de propiedad intelectual y volvemos a abrirlo al público.', 'xrcb-plugin'),
				),
				'allow_null' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'podcast',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array(
		'id' => 'acf_live',
		'title' => 'Podcast o Broadcast',
		'fields' => array(
			array(
				'key' => 'field_5cff743673e4e',
				'label' => '',
				'name' => 'live',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'false' => 'Podcast',
					'true' => 'Live Broadcast',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'default_value' => 'false',
				'layout' => 'horizontal',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5cff743673aaa',
				'label' => 'Live Broadcast Duration',
				'name' => 'live_duration',
				'type' => 'number',
				'instructions' => 'Duración en secundos',
				'required' => 1,
				'conditional_logic' => [
                    [
                        [
                            "field" => "field_5cff743673e4e",
                            "operator" => "==",
                            "value" => true
                        ]
                    ]
                ],
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'podcast',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	register_field_group(array (
		'id' => 'acf_resource',
		'title' => 'Recurs',
		'fields' => array (
			array (
				'key' => 'field_xxxa115e1e33f',
				'label' => 'URL',
				'name' => 'url',
				'type' => 'url',
				'required' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'resource',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	register_field_group(array (
		'id' => 'acf_repost',
		'title' => 'Repost',
		'fields' => array (
			array (
				'key' => 'field_60994e01264fb',
				'label' => 'Podcast',
				'name' => 'podcast',
				'type' => 'post_object',
				'required' => 1,
				'post_type' => array (
					0 => 'podcast',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'repost',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

/**
 * ACF posts LIKE filter 
 */
/*function xrcb_posts_where( $where ) {
	
	$where = str_replace("meta_key = 'podcast_$", "meta_key LIKE 'podcast_%", $where);

	return $where;
}
add_filter('posts_where', 'xrcb_posts_where');*/

/**
 * save automatic repost title 
 */
function xrcb_set_repost_title( $post_id ) {
	if (get_post_type($post_id) == 'repost' && get_field('podcast', $post_id)) {
		$podcast = get_field('podcast', $post_id);
		$post_title = $podcast->post_title;

		remove_action('save_post', 'xrcb_set_repost_title');

		wp_update_post(array(
			'ID' => $post_id, 
			'post_title' => $post_title,
			'post_date' => $podcast->post_date,
			'post_name' => ''
		));

	    add_action('save_post', 'xrcb_set_repost_title');
	}
}
add_filter( 'save_post' , 'xrcb_set_repost_title' , '10', 1 );


/**
 * change podcast title label
 */
function xrcb_podcast_change_title_label( $field ) {
	
    $field['label'] = __("Posa un títol al podcast", "xrcb-plugin");
    //$field['instructions'] = "Changed Instruction";

    return $field;
}
add_filter('acf/prepare_field/name=_post_title', 'xrcb_podcast_change_title_label');

/**
 * remove podcast category taxonomy
 */
function xrcb_remove_default_podcast_category_metabox() {
	remove_meta_box( 'tagsdiv-podcast_category', 'podcast', 'normal' );
	remove_meta_box( 'tagsdiv-podcast_tag', 'podcast', 'normal' );
	remove_meta_box( 'tagsdiv-podcast_programa', 'podcast', 'normal' );
	remove_meta_box( 'tagsdiv-podcast_radio', 'podcast', 'normal' );
}
add_action( 'admin_menu' , 'xrcb_remove_default_podcast_category_metabox' );

/**
 * limit Radios to 3 categories
 */
add_filter('acf/validate_value/name=categories', 'only_allow_3', 20, 4);
function only_allow_3($valid, $value, $field, $input) {
  if (count($value) > 3) {
    $valid = 'No més que 3 categories estan permeses.';
  }
  return $valid;
}

/**
 * add google maps API key -> substitute by Openlayers map
 */
function xrcb_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyAk5XwRCBSoh65Mtc5lMZYxnxmjhKZ5NfY';
	return $api;	
}
add_filter('acf/fields/google_map/api', 'xrcb_acf_google_map_api');

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
//define('ACF_EARLY_ACCESS', '5');

/**
 * populate neighbourhood field once city is selected (Radio Post)
 */
function xrcb_admin_enqueue( $hook ) {
  $type = get_post_type(); // Check current post type
  $types = array( 'radio' ); // Allowed post types
 
  if( !in_array( $type, $types ) )
      return; // Only applies to radio types in array
 
  wp_enqueue_script( 'populate-area', get_stylesheet_directory_uri() . '/js/autopopulate.js' );
 
  wp_localize_script( 'populate-area', 'pa_vars', array(
        'pa_nonce' => wp_create_nonce( 'pa_nonce' ), // Create nonce which we later will use to verify AJAX request
      )
  );
}
add_action( 'admin_enqueue_scripts', 'xrcb_admin_enqueue' );

/**
 * define capabilities
 */
function xrcb_add_cap_upload() {
	// admin
	$role = get_role( 'administrator' );
	$role->add_cap( 'edit_radios' ); 
	$role->add_cap( 'edit_published_radios' ); 
	$role->add_cap( 'edit_private_radios' ); 
	$role->add_cap( 'edit_others_radios' ); 
	$role->add_cap( 'publish_radios' ); 
	$role->add_cap( 'read_private_radios' ); 
	$role->add_cap( 'delete_private_radios' ); 
	$role->add_cap( 'delete_published_radios' ); 
	$role->add_cap( 'delete_others_radios' ); 

	$role->add_cap( 'edit_podcasts' ); 
	$role->add_cap( 'edit_published_podcasts' ); 
	$role->add_cap( 'edit_private_podcasts' ); 
	$role->add_cap( 'edit_others_podcasts' ); 
	$role->add_cap( 'publish_podcasts' ); 
	$role->add_cap( 'read_private_podcasts' ); 
	$role->add_cap( 'delete_private_podcasts' ); 
	$role->add_cap( 'delete_published_podcasts' ); 
	$role->add_cap( 'delete_others_podcasts' ); 

	$role->add_cap( 'edit_resources' ); 
	$role->add_cap( 'edit_published_resources' ); 
	$role->add_cap( 'edit_private_resources' ); 
	$role->add_cap( 'edit_others_resources' ); 
	$role->add_cap( 'publish_resources' ); 
	$role->add_cap( 'read_private_resources' ); 
	$role->add_cap( 'delete_private_resources' ); 
	$role->add_cap( 'delete_published_resources' ); 
	$role->add_cap( 'delete_others_resources' ); 

	$role->add_cap( 'edit_reposts' ); 
	$role->add_cap( 'edit_published_reposts' ); 
	$role->add_cap( 'edit_private_reposts' ); 
	$role->add_cap( 'edit_others_reposts' ); 
	$role->add_cap( 'publish_reposts' ); 
	$role->add_cap( 'read_private_reposts' ); 
	$role->add_cap( 'delete_private_reposts' ); 
	$role->add_cap( 'delete_published_reposts' ); 
	$role->add_cap( 'delete_others_reposts' ); 

	// editor
	$role = get_role( 'editor' );
	$role->add_cap( 'edit_radios' ); 
	$role->add_cap( 'edit_published_radios' ); 
	$role->add_cap( 'edit_private_radios' ); 
	$role->add_cap( 'edit_others_radios' ); 
	$role->add_cap( 'publish_radios' ); 
	$role->add_cap( 'read_private_radios' ); 
	$role->add_cap( 'delete_radios' ); 
	$role->add_cap( 'delete_private_radios' ); 
	$role->add_cap( 'delete_published_radios' ); 
	$role->add_cap( 'delete_others_radios' ); 

	$role->add_cap( 'edit_podcasts' ); 
	$role->add_cap( 'edit_published_podcasts' ); 
	$role->add_cap( 'edit_private_podcasts' ); 
	$role->add_cap( 'edit_others_podcasts' ); 
	$role->add_cap( 'publish_podcasts' ); 
	$role->add_cap( 'read_private_podcasts' ); 
	$role->add_cap( 'delete_private_podcasts' ); 
	$role->add_cap( 'delete_podcasts' ); 
	$role->add_cap( 'delete_published_podcasts' ); 
	$role->add_cap( 'delete_others_podcasts' ); 

	$role->add_cap( 'edit_resources' ); 
	$role->add_cap( 'edit_published_resources' ); 
	$role->add_cap( 'edit_private_resources' ); 
	$role->add_cap( 'edit_others_resources' ); 
	$role->add_cap( 'publish_resources' ); 
	$role->add_cap( 'read_private_resources' ); 
	$role->add_cap( 'delete_resources' ); 
	$role->add_cap( 'delete_private_resources' ); 
	$role->add_cap( 'delete_published_resources' ); 
	$role->add_cap( 'delete_others_resources' );  

	$role->add_cap( 'edit_reposts' ); 
	$role->add_cap( 'edit_published_reposts' ); 
	$role->add_cap( 'edit_private_reposts' ); 
	$role->add_cap( 'edit_others_reposts' ); 
	$role->add_cap( 'publish_reposts' ); 
	$role->add_cap( 'read_private_reposts' ); 
	$role->add_cap( 'delete_private_reposts' ); 
	$role->add_cap( 'delete_published_reposts' ); 
	$role->add_cap( 'delete_others_reposts' ); 

	// author
	$role = get_role( 'author' );
	$role->add_cap( 'publish_posts' );
	$role->add_cap( 'edit_posts' );
	$role->add_cap( 'edit_published_posts' ); 
	$role->add_cap( 'delete_posts' );
	$role->add_cap( 'upload_files' ); 
	$role->add_cap( 'edit_radios' );

	$role->add_cap( 'create_podcasts' );
	$role->add_cap( 'publish_podcasts' );
	$role->add_cap( 'delete_podcasts' );
	$role->add_cap( 'edit_podcasts' );
	$role->add_cap( 'edit_published_podcasts' );
	$role->add_cap( 'delete_published_podcasts' );

	$role->add_cap( 'edit_reposts' ); 
	$role->add_cap( 'edit_published_reposts' ); 
	$role->add_cap( 'publish_reposts' ); 
	$role->add_cap( 'delete_published_reposts' ); 

	// contributor
	$role = get_role( 'contributor' );
	$role->remove_cap( 'publish_posts' );	// contradicción con programación
	$role->remove_cap( 'edit_posts' );		// contradicción con programación
	$role->remove_cap( 'edit_published_posts' ); 
	$role->remove_cap( 'delete_posts' );
	$role->remove_cap( 'publish_radios' ); 

	/*$role->add_cap( 'upload_files' );
	$role->add_cap( 'edit_radios' );

	$role->add_cap( 'create_podcasts' );
	$role->add_cap( 'publish_podcasts' );
	$role->add_cap( 'delete_podcasts' );
	$role->add_cap( 'edit_podcasts' );
	$role->add_cap( 'edit_published_podcasts' );
	$role->add_cap( 'delete_published_podcasts' );*/

	$role->remove_cap( 'upload_files' );
	$role->remove_cap( 'edit_radios' );

	$role->remove_cap( 'create_podcasts' );
	$role->remove_cap( 'publish_podcasts' );
	$role->remove_cap( 'delete_podcasts' );
	$role->remove_cap( 'edit_podcasts' );
	$role->remove_cap( 'edit_published_podcasts' );
	$role->remove_cap( 'delete_published_podcasts' );

	$role->remove_cap( 'edit_reposts' );
	$role->remove_cap( 'edit_published_reposts' );
	$role->remove_cap( 'publish_reposts' );
	$role->remove_cap( 'delete_published_reposts' );

	// artist
	/*$role = get_role( 'artist' );
	//$role->remove_cap( 'publish_posts' );
	//$role->remove_cap( 'edit_posts' );
	//$role->remove_cap( 'delete_posts' );
	$role->add_cap( 'upload_files' ); 
	$role->add_cap( 'create_resource' );
	$role->add_cap( 'create_resources' );
	$role->add_cap( 'publish_resource' );
	$role->add_cap( 'publish_resources' );
	$role->add_cap( 'edit_resource' ); 
	$role->add_cap( 'edit_published_resources' ); 
	$role->add_cap( 'edit_private_resources' ); 
	$role->add_cap( 'edit_others_resources' ); 
	$role->add_cap( 'publish_resources' ); 
	$role->add_cap( 'read_private_resources' ); 
	$role->add_cap( 'delete_resources' ); 
	$role->add_cap( 'delete_private_resources' ); 
	$role->add_cap( 'delete_published_resources' ); 
	$role->add_cap( 'delete_others_resources' );*/
}
add_action( 'init', 'xrcb_add_cap_upload' );

/**
 * add admin menu for programación
 */
function xrcb_add_admin_menu(){
    global $wp_admin_bar;
    $wp_admin_bar->add_node(
        array(
            'id'        => 'programacio-podcast-menu',
            'title'     => 'Programació Podcast',
            'href'      => site_url() . '/programacio-de-emissions/',
            'parent'    => 'new-content',
            //'meta'      => array( 'class' => 'my-custom-class' ),
        )
    );
    $wp_admin_bar->add_node(
        array(
            'id'        => 'programacio-live-menu',
            'title'     => 'Programació Live',
            'href'      => site_url() . '/programacio-live/',
            'parent'    => 'new-content',
            //'meta'      => array( 'class' => 'my-custom-class' ),
        )
    );
}
add_action( 'wp_before_admin_bar_render', 'xrcb_add_admin_menu' );

/**
 * hide post radio metaboxes
 */
// no va, por ahora los escondo por css
/*function xrcb_remove_meta_box($hidden, $screen) {
	// only for non-administrators
	if ( ! current_user_can( 'manage_options' ) ) {
	    //remove_meta_box( 'tagsdiv-post_tag', 'radio', 'normal' );
	    remove_meta_box( 'wpm-radio-languages', 'radio', 'side' );
	}
}
add_action('add_meta_boxes','xrcb_remove_meta_box');*/

/**
 * Populate post_content field before saving post
 * Populate radio field if empty (=user has only one radio)
 */
add_action('acf/save_post', 'xrcb_podcast_save_post');
function xrcb_podcast_save_post( $post_id ) {

	remove_filter('acf/save_post', 'xrcb_podcast_save_post');

	// only for non administrators
	if (!current_user_can('manage_options')) {

		// manually set radio podcast belonging to, to users first radio station
		$meta_query = new WP_Query( array(
			'author' => get_current_user_id(),
			'post_type' => 'radio',
			'post_status' => 'publish',
		));
		$radios = $meta_query->get_posts();
		//trigger_error(json_encode(count($radios)), E_USER_WARNING);
		
		// automatically set first radio as podcast owner
		if (($radios && count($radios) == 1) || $_POST['acf']['field_5b2a14e0b2380'] == '') {
			//$_POST['acf']['field_5b2a14e0b2380'] = strval($radios[0]->ID);
			update_field('field_5b2a14e0b2380', strval($radios[0]->ID), $post_id);
		}
	}

	// update post content field with description
	$post = get_post($post_id);
	$post->post_content = stripslashes($_POST['acf']['field_5a685849efhhh']);
	wp_update_post($post);

	add_action('acf/save_post', 'xrcb_podcast_save_post');

	return $post_id;
}

/**
 * show only radios belonging to actual user (at podcast form)
 */
add_filter('acf/fields/post_object/query/name=radio', 'xrcb_filter_podcast_radios', 20, 3);
function xrcb_filter_podcast_radios($args, $field, $post_id) {

	// only for non administrators
	if (!current_user_can('manage_options')) {

		// get all programs belonging to user
		$meta_query = new WP_Query( array(
			'author' => get_current_user_id(),
			'post_type' => 'radio',
			'post_status' => 'publish',
		));
		$radios = $meta_query->get_posts();

		foreach ($radios as $radio) {
			// show only this radios
		    $args['include'][] = $radio->ID;
		}
	}

  	return $args;
}

/**
 * show only programs belonging to actual user (at podcast form)
 */
add_filter('acf/fields/taxonomy/query/name=programes', 'xrcb_filter_podcast_programs', 20, 3);
function xrcb_filter_podcast_programs($args, $field, $post_id) {

	// only for non administrators
	if (!current_user_can('manage_options')) {

		// get all programs belonging to user
		$programs = get_terms(array(
			'taxonomy' => 'podcast_programa',
			'hide_empty' => false,
			'meta_key' => 'usuari',
		    'meta_value' => get_current_user_id(),
		));

		foreach ($programs as $program) {
			// show only this programs
		    $args['include'][] = $program->term_id;
		}
	}

  	return $args;
}

/**
 * show only programs belonging to actual user (at podcast form)
 */
add_filter('create_term', 'xrcb_create_podcast_programs', 20, 3);
function xrcb_create_podcast_programs($term_id, $tt_id, $taxonomy) {
	update_field("usuari", get_current_user_id(), "podcast_programa_".$term_id);
}

// add custom columns to podcast backend table
add_filter('manage_podcast_posts_columns', 'xrcb_podcast_columns');
function xrcb_podcast_columns($columns) {
    $columns['radio'] = 'Radio';
    $columns['program'] = 'Programa';
    return $columns;
}

add_action('manage_podcast_posts_custom_column',  'xrcb_show_podcast_columns', 10, 2);
function xrcb_show_podcast_columns($name, $post_id) {
    switch ($name) {
        case 'radio':
        	$radio_tmp = get_field('radio', $post_id);
        	if ($radio_tmp) {
				echo $radio_tmp->post_title;
			}
            break;
        case 'program':
        	$programa_tmp = get_the_terms($post_id, 'podcast_programa');
        	if ($programa_tmp && count($programa_tmp) == 1) {
				echo $programa_tmp[0]->name;
			}
            break;
    }
}

/* add message on top of new podcast form */
function xrcb_podcast_message() {
	if (get_current_screen()->id == 'podcast')
	    echo '<p style="color:red;">'. esc_html__('Este formulario es obsoleto, por favor utiliza el formulario new: ', 'xrcb') . '<a href="https://xrclab.cat/ca/publish-podcast/">https://xrclab.cat/ca/publish-podcast/</a></p>';
}
add_action( 'edit_form_top', 'xrcb_podcast_message');