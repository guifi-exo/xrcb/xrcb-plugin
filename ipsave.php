<?php

require_once '../../../wp-config.php';
global $wpdb;

// setup query argument to get all radios
$radio_args = array(
        'post_type' => 'radio',
        'post_status' => 'publish',
        'posts_per_page' => -1,
);
$radio_query = new WP_Query($radio_args);
$radios = $radio_query->get_posts();

foreach ($radios as $key => $radio) {
        if (get_field('ipsave', $radio->ID)) {

                // get all podcasts from ipsave radio
                $podcast_args = array(
                        'post_type' => 'podcast',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_key' => 'radio',
                        'meta_value' => $radio->ID,
                );
                $podcast_query = new WP_Query($podcast_args);
                $podcasts = $podcast_query->get_posts();

                foreach ($podcasts as $key => $podcast) {
                        $file = get_post_meta($podcast->ID, 'file_mp3', true);
                        if ($file) {
                                $url = wp_get_attachment_url($file);
                                if ($url) {
                                        echo $url . PHP_EOL;
                                }
                        }
                }
        }
        else {
                // get all podcasts which are ipsave for not ipsave radios
                $podcast_args = array(
                        'post_type' => 'podcast',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_query' => array(
                                array(
                                        'key' => 'radio',
                                        'value' => $radio->ID,
                                ),
                                array(
                                        'key' => 'ipsave_podcast',
                                        'value' => 'yes',
                                        'compare' => 'LIKE',
                                )
                        )
                );
                $podcast_query = new WP_Query($podcast_args);
                $podcasts = $podcast_query->get_posts();

                foreach ($podcasts as $key => $podcast) {
                        $file = get_post_meta($podcast->ID, 'file_mp3', true);
                        if ($file) {
                                $url = wp_get_attachment_url($file);
                                if ($url) {
                                        echo $url . PHP_EOL;
                                }
                        }
                }
        }
}
?>